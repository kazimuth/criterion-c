#include "criterion.h"
#include <string>

int main()
{
    std::string x("hello, ");
    criterion::benchmark("cplusplus", [&]() {
        std::string y("world!");
        volatile std::string result = x + y;
    });
    criterion::benchmark("cplusplus overhead", [&]() {});
    criterion::summary();
}