#include "criterion.h"
#include <stdlib.h>
#include <string.h>

void test_benchmark(void *_value)
{
    volatile char data[] = "hello, world! how are you today? I'm doing just fine, I must say :)";
    char *result = (char *)malloc(sizeof(data));

    for (int i = 0; i < sizeof(data); i++)
    {
        result[i] = data[i];
    }

    free(result);
}

void test_overhead(void *unused) {}

int main()
{
    int value = 3;
    criterion_benchmark("c", &value, test_benchmark);
    criterion_benchmark("c overhead", 0, test_overhead);
}