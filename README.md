# criterion-c

A dead-simple, statistically reliable benchmarking library for C and C++. It automatically warms-up your code, filters outliers, and performs statistical tests to see if the performance of your code has changed between runs.

Consists of C and C++ bindings to the excellent [`criterion-rs`](https://github.com/bheisler/criterion.rs/) benchmarking library.

# usage

```cpp
#include "criterion.h"

int main() {
    std::string x("hello, ");
    criterion::benchmark("example-benchmark", [&]() {
        std::string y("world!");

        // use "volatile" to prevent dead code elimination from removing benchmark code
        volatile std::string result = x + y;
    });
    criterion::benchmark("other-benchmark", [&]() {});
    criterion::summary();
}
```

```sh
# build:
c++ bench.c -o bench -I path/to/criterion-c/include -L path/to/criterion-c/lib \
     -lcriterion -lpthread -lm -ldl -O3 -std=c++11 # other CFLAGS...

# make sure you build in release mode! (-O3)
# make sure you use at least -std=c++11 as well

# run a single benchmark:
./bench example-benchmark
# run all benchmarks:
./bench
# see other command-line arguments:
./bench --help
```

Sample output:

```
Benchmarking example-benchmark
Benchmarking example-benchmark: Warming up for 3.0000 s
Benchmarking example-benchmark: Collecting 100 samples in estimated 5.0001 s (146M iterations)
Benchmarking example-benchmark: Analyzing
example-benchmark       time:   [33.998 ns 34.607 ns 35.390 ns]
                        change: [-7.5597% -4.0353% -0.3488%] (p = 0.03 < 0.05)
                        Change within noise threshold.
```

Note that criterion-c will track the performance of your code over time;
it will save results in the directory `target/criterion` relative to wherever you run your executable.

Note also that the library adds about 2 nanoseconds of overhead to benchmarked code. Be aware of this if you're benchmarking very small operations!

A static library for linux x86_64 is provided.

To build from scratch, you'll need the [rust toolchain](https://rustup.rs/).

```sh
cd rust && cargo build --release && cd ..
```

This will produce `rust/target/release/libcriterion.a`.

# docs

```cpp
namespace criterion {

/// Benchmark a function.
/// - name: a human-readable name for the benchmark
/// - bench: a function / closure / functor to call. Will be called many times. Can have state.
template<typename Bench>
inline void benchmark(const char *name, Bench &bench);

/// Print a summary after all benchmarks have been run.
inline void summary();
};
```

For more documentation, see the [criterion-rs manual](https://bheisler.github.io/criterion.rs/book/criterion_rs.html).

Specifically, the [analysis process](https://bheisler.github.io/criterion.rs/book/analysis.html) section of the manual explains the statistical analysis performed by criterion-c.
