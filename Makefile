
all: lib/libcriterion.a

lib/libcriterion.a: rust/src/*.rs rust/Cargo.*
	cd rust && cargo build --release && cd ..
	cp rust/target/release/libcriterion.a lib/libcriterion.a
	
test: lib/test/c lib/test/cplusplus
	lib/test/c
	lib/test/cplusplus

FLAGS = -lcriterion -Iinclude -Llib -lpthread -lm -ldl -O3
ifeq ($(shell uname -s),Darwin)
	FLAGS += -framework Security
endif

CPPFLAGS = -std=c++11

lib/test: 
	mkdir -p lib/test

lib/test/c: test/c.c lib/libcriterion.a lib/test include/*
	cc test/c.c -o lib/test/c $(FLAGS)

lib/test/cplusplus: test/cplusplus.cpp lib/libcriterion.a lib/test include/*
	c++ test/cplusplus.cpp -o lib/test/cplusplus $(FLAGS) $(CPPFLAGS)
