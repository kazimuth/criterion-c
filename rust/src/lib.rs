extern crate criterion;
extern crate lazy_static;

use std::ffi::{c_void, CStr};
use std::os::raw::c_char;
use std::ptr;

use criterion::Criterion;

fn init_criterion() -> Criterion {
    Criterion::default().configure_from_args()
}

#[no_mangle]
pub extern "C" fn criterion_benchmark(
    name: *const c_char,
    payload: *mut c_void,
    cb: extern "C" fn(*mut c_void) -> (),
) {
    assert!(name != ptr::null(), "null benchmark name");

    let name = unsafe {
        CStr::from_ptr(name)
            .to_str()
            .expect("invalid utf-8 in benchmark name")
    };

    init_criterion().bench_function(name, move |b| {
        b.iter(move || {
            cb(payload);
        })
    });
}

#[no_mangle]
pub extern "C" fn criterion_summary() {
    init_criterion().final_summary();
}
